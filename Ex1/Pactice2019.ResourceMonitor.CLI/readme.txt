﻿PREREQ:
	.NET Framework 4.7.2

INSTALL AS A SERVICE (OPTIONAL):
	rsmon.exe install

USAGE:
	rsmon.exe -target:<Path_To_Observed_Dir> -output:<Parth_To_Report> [--verbose]
EXAMPLE
	rsmon.exe -target:C:\tmp\data -output:C:\tmp\result.txt --verbose
