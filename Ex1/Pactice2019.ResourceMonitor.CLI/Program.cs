﻿﻿using System;
using Castle.Windsor;
using Pactice2019.ResourceMonitor.CLI.Windsor;
using Pactice2019.ResourceMonitor.Engines;
using Topshelf;

namespace Pactice2019.ResourceMonitor.CLI
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      // Configure service container.
      var serviceContainer = new WindsorContainer();
      serviceContainer.Install(new ResourceMonitorInstaller());

      // Configure and run application host.
      var ec = HostFactory.Run(configuration =>
      {
        var observedDirectoryPath = "";
        var observedDirectoryChangesFilePath = "";

        var isVerbosed = false;
        var isClean = false;

        // Usage: rsmon -target:<path_to_obseved_dir>
        configuration.AddCommandLineDefinition("target", arg => observedDirectoryPath = arg);

        // Usage: rsmon -target:<path_to_obseved_dir> -output:<path_to_output_file>
        configuration.AddCommandLineDefinition("output", arg => observedDirectoryChangesFilePath = arg);

        // Usage: rsmon -target:<path_to_obseved_dir> -output:<path_to_output_file> --verbose
        configuration.AddCommandLineSwitch("verbose", sw => isVerbosed = sw);

        // Usage: rsmon -target:<path_to_obseved_dir> -output:<path_to_output_file> --clean
        configuration.AddCommandLineSwitch("clean", sw => isClean = sw);

        configuration.ApplyCommandLine();

        configuration.Service<IMonitoringEngine>(s =>
        {
          s.ConstructUsing(name => serviceContainer.Resolve<IMonitoringEngine>());
          s.WhenStarted(scannerEngine => scannerEngine.Attach(observedDirectoryPath, observedDirectoryChangesFilePath, isVerbosed, isClean));
          s.WhenStopped((scannerEngine) =>
          {
            // TODO: Dispose smth.
          });
        });

        configuration.RunAsLocalSystem();
        configuration.SetServiceName("ResourceMonitorService");
        configuration.SetDescription("Monitoring for specified location.");
        configuration.SetDisplayName("Resource Monitor");
      });

      Environment.ExitCode = (int) ec;
    }
  }
}
