﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Alba.CsCss.Style;

namespace Pactice2019.ResourceMonitor.Operations
{
  public class ValidateCssOperation 
    : MonitoringOperation
  {
    public static readonly object SSync = new object();

    public const string ValidCssMessage = "Valid CSS.";
    public const string NotValidCssMessage = "Invalid CSS.";

    private volatile List<string> _errors;
    private readonly string[] _supportedExtensions = { ".css" };

    public ValidateCssOperation() : base(typeof(ValidateCssOperation).Name)
    {
      _errors = new List<string>();
    }

    public override string Perform(Stream input)
    {
      lock (SSync)
      {
        _errors.Clear();

        string css;
        using (var sr = new StreamReader(input))
        {
          css = sr.ReadToEnd();
        }

        var cssLoader = new CssLoader();
        cssLoader.ParseError += (s, e) => { _errors.Add($"{e.LineNumber}: {e.Message}"); };
        cssLoader.ParseSheet(css, new Uri("http://somewhere.com/test.css"), new Uri("http://somewhere.com"));

        return _errors.Count == 0 ? ValidCssMessage : NotValidCssMessage;
      }
    }

    public override bool CanPerform(string resource)
    {
      return _supportedExtensions.Contains(Path.GetExtension(resource));
    }
  }
}