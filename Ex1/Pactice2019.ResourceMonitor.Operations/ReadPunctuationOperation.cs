﻿using System.IO;
using System.Text.RegularExpressions;

namespace Pactice2019.ResourceMonitor.Operations
{
  public class ReadPunctuationOperation 
    : MonitoringOperation
  {
    private const string RgxPattern = @"\p{P}";

    public ReadPunctuationOperation() : base(typeof(ReadPunctuationOperation).Name)
    {
      IsDefault = true;
    }
    public override string Perform(Stream input)
    {
      string str;
      using (var sr = new StreamReader(input))
      {
        str = sr.ReadToEnd();
      }

      return $"{new Regex(RgxPattern).Matches(str).Count}";
    }

    public override bool CanPerform(string resource)
    {
      return true;
    }
  }
}