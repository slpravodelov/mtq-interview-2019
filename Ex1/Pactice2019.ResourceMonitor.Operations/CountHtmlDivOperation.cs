﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

using CsQuery;

namespace Pactice2019.ResourceMonitor.Operations
{
  public class CountHtmlDivOperation 
    : MonitoringOperation
  {
    public static readonly object SSync = new object();

    private readonly string[] _supportedExtensions = {".htm", ".html"};

    public CountHtmlDivOperation() : base(typeof(CountHtmlDivOperation).Name)
    {
    }

    public override string Perform(Stream input)
    {
      lock (SSync)
      {
        string html;
        using (var sr = new StreamReader(input))
        {
          html = sr.ReadToEnd();
        }

        var parser = CQ.Create(html, HtmlParsingMode.Content, HtmlParsingOptions.IgnoreComments);

        var count = 0;
        Count(parser.Elements, ref count);
        return $"{count}";
      }
    }

    public static void Count(IEnumerable<IDomElement> elements, ref int count)
    {
      var domElements = elements.ToList();
      if (!domElements.Any()) return;

      count += domElements.Count(e => e.NodeName == "DIV");
      foreach (var domElement in domElements)
      {
        Count(domElement.ChildElements, ref count);
      }
    }

    public override bool CanPerform(string resource)
    {
      return _supportedExtensions.Contains(Path.GetExtension(resource));
    }
  }
}
