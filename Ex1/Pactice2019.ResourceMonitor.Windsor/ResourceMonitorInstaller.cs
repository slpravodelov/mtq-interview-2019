﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Pactice2019.ResourceMonitor.Engines;
using Pactice2019.ResourceMonitor.Operations;
using Pactice2019.ResourceMonitor.Services;

namespace Pactice2019.ResourceMonitor.CLI.Windsor
{
  /// <summary>
  /// Represents Windsor installer for application.
  /// </summary>
  public class ResourceMonitorInstaller
    : IWindsorInstaller
  {
    /// <summary>
    /// Install services.
    /// </summary>
    /// <param name="container">Target container.</param>
    /// <param name="store">Container configuration.</param>
    public void Install(IWindsorContainer container, IConfigurationStore store)
    {
      container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));

      // Services

      container.Register(Component
        .For<IMonitoringEngine>()
        .ImplementedBy<FileStreamMonitor>()
        .LifestyleSingleton());

      container.Register(Component
        .For<IReportingService>()
        .ImplementedBy<FileStreamReportingService>()
        .LifestyleTransient());

      container.Register(Component
        .For<ILogService>()
        .ImplementedBy<ConsoleLogService>()
        .LifestyleTransient());

      container.Register(Component
        .For<IRepository<MonitoringOperation>>()
        .ImplementedBy<MonitoringOperationRepository>()
        .LifestyleTransient());

      // Operation Services

      container.Register(Component
        .For<MonitoringOperation>()
        .ImplementedBy<ReadPunctuationOperation>()
        .LifestyleTransient());

      container.Register(Component
        .For<MonitoringOperation>()
        .ImplementedBy<CountHtmlDivOperation>()
        .LifestyleTransient());

      container.Register(Component
        .For<MonitoringOperation>()
        .ImplementedBy<ValidateCssOperation>()
        .LifestyleTransient());
    }
  }
}
