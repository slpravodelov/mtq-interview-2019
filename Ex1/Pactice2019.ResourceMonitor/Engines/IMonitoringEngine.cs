﻿using System;
using System.IO;

namespace Pactice2019.ResourceMonitor.Engines
{
  /// <summary>
  /// Represents a stream scanner engine.
  /// </summary>
  public interface IMonitoringEngine : IDisposable
  {
    /// <summary>
    /// Indicates verbose mode.
    /// </summary>
    bool IsVerbose { get; set; }

    #region Methods

    /// <summary>
    /// Attach scanner to path or URI
    /// </summary>
    /// <param name="targetUri">Target path or service URI.</param>
    /// <param name="reportUri">Output path or service uri.</param>
    /// <param name="verbose">Indicates that tracing is enabled.</param>
    /// <param name="clean">Clean up target URI.</param>
    void Attach(string targetUri, string reportUri, bool verbose = false, bool clean = false);

    /// <summary>
    /// Get operations report as stream.
    /// </summary>
    /// <returns></returns>
    Stream GetReportStream();

    /// <summary>
    /// Clear current operation report.
    /// </summary>
    void ClearReport();

    #endregion
  }
}