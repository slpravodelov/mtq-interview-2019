﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Pactice2019.ResourceMonitor.Operations;
using Pactice2019.ResourceMonitor.Services;

namespace Pactice2019.ResourceMonitor.Engines
{
  /// <inheritdoc />
  public class FileStreamMonitor
    : MonitoringEngine
  {
    #region Fields

    private bool _disposed;
    private readonly FileSystemWatcher _fsWatcher = new FileSystemWatcher();

    #endregion

    #region Constructors

    /// <inheritdoc />
    public FileStreamMonitor(
      IReportingService reportService, 
      ILogService logService,
      IRepository<MonitoringOperation> operationsRepository)
      : base(reportService, logService, operationsRepository)
    {
    }

    #endregion

    #region Methods

    private async void OnChanged(object sender, FileSystemEventArgs e)
    {
      try
      {
        // Disable event rising.
        _fsWatcher.EnableRaisingEvents = false;

        while (!IsResourceReady(e.FullPath))
        {
          await Task.Delay(500);
        }

        if (IsVerbose) Log.Info($"File change handled by thread {Thread.CurrentThread.ManagedThreadId}");

        // Read file to buffer and release file handler.
        using (var fileStream = File.Open(e.FullPath, FileMode.Open, FileAccess.Read, FileShare.Read))
        {
          base.OnResourceChanged(e.FullPath, fileStream);
        }
      }
      catch (Exception ex)
      {
        Log.Error(ex);
      }
      finally
      {
        // Enable event rising.
        _fsWatcher.EnableRaisingEvents = true;
      }
    }

    private static bool IsResourceReady(string resourceUri)
    {
      try
      {
        using (File.Open(resourceUri, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
          return true;
        }
      }
      catch (IOException)
      {
        return false;
      }
    }

    /// <inheritdoc />
    public sealed override void Attach(string targetUri, string reportUri, bool verbose = false, bool clean = false)
    {
      if (!Directory.Exists(targetUri))
      {
        throw new DirectoryNotFoundException($"Resource or directory '{targetUri}' does not exist. There is nothing to observe.");
      }
      base.Attach(targetUri, reportUri, verbose, clean);

      _fsWatcher.Path = targetUri;
      _fsWatcher.NotifyFilter = NotifyFilters.Size | NotifyFilters.FileName;
      _fsWatcher.Created += OnChanged;
      _fsWatcher.EnableRaisingEvents = true;
    }

    public override Stream GetReportStream()
    {
      return Report.GetStream(OutputUri);
    }

    protected override void Dispose(bool disposing)
    {
      if (_disposed)
      {
        return;
      }

      if (disposing)
      {
        _fsWatcher.Dispose();
      }

      _disposed = true;
    }

    #endregion
  }
}