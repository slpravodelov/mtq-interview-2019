﻿using System;
using System.IO;
using System.Linq;
using Pactice2019.ResourceMonitor.Operations;
using Pactice2019.ResourceMonitor.Services;

namespace Pactice2019.ResourceMonitor.Engines
{ 
  /// <summary>
  /// Represents a stream scanner engine.
  /// </summary>
  public abstract class MonitoringEngine : IMonitoringEngine
  {
    private readonly IRepository<MonitoringOperation> _operationsRepository;

    #region Constructors

    /// <summary>
    /// Initializes a new instance of scanner object.
    /// </summary>
    /// <param name="reportService">Operation results service.</param>
    /// <param name="logService">Evant log and diagnostics service.</param>
    protected MonitoringEngine(IReportingService reportService, ILogService logService, IRepository<MonitoringOperation> operationsRepository)
    {
      _operationsRepository = operationsRepository;

      Report = reportService;
      Log = logService;
    }

    #endregion

    #region Properties

    /// <summary>
    /// Target directory path.
    /// </summary>
    protected string TargetPath { get; set; }

    /// <summary>
    /// Output path.
    /// </summary>
    protected string OutputUri { get; set; }

    /// <summary>
    /// Operation result service.
    /// </summary>
    protected IReportingService Report { get; }

    /// <summary>
    /// Event log service.
    /// </summary>
    protected ILogService Log { get; }

    /// <inheritdoc />
    public bool IsVerbose { get; set; } 

    #endregion

    #region Methods

    /// <inheritdoc />
    public virtual void Attach(string targetUri, string reportUri, bool verbose = false, bool clean = false)
    {
      if (clean)
      {
        if (Directory.Exists(targetUri))
        {
          Directory.Delete(targetUri);
          Directory.CreateDirectory(targetUri);
        }
        Report.CleanUp(reportUri);
      }

      Report.Configure(reportUri);

      if (verbose) Log.Info($"Attaching monitor to {targetUri} . . .");

      IsVerbose = verbose;
      OutputUri = reportUri;
      TargetPath = targetUri;

      if (verbose) Log.Info($"Monitoring:\n\tTARGET: {targetUri}\n\tOUTPUT: {OutputUri}");
    }

    /// <inheritdoc />
    public abstract Stream GetReportStream();

    /// <inheritdoc />
    public void ClearReport()
    {
      Report.CleanUp(OutputUri);
    }

    protected virtual void OnResourceChanged(string resourceUri, Stream input)
    {
      if (IsVerbose)
      {
        Log.Info($"Caught changes: {resourceUri}");
      }

      var ops = _operationsRepository.Get(o => o.CanPerform(resourceUri) && !o.IsDefault).ToList();
      foreach (var op in (ops.Any() ? ops : _operationsRepository.Get(o => o.IsDefault)))
      {
        Report.Write(op, OutputUri, resourceUri, op.Perform(input));

        if (IsVerbose)
        {
          Log.Info($"Operation '{op.Name}' performed on {resourceUri}");
        }
      }
    }

    /// <inheritdoc />
    public virtual void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Dispose managed/unmanaged resources.
    /// </summary>
    /// <param name="disposing">Indicates call System.IDisposable.Dispose().</param>
    protected abstract void Dispose(bool disposing);

    #endregion

    ~MonitoringEngine()
    {
      Dispose(false);
    }
  }
}
