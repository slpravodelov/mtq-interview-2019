﻿using System;
using System.Collections.Generic;

namespace Pactice2019.ResourceMonitor
{
  public interface IRepository<TEntity>
    where TEntity : class
  {
    int? Create(TEntity item);
    TEntity FindById(string id);
    IEnumerable<TEntity> Get();
    IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);
    void Remove(TEntity item);
    void Update(TEntity item);
  }
}