﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pactice2019.ResourceMonitor.Operations
{
  public sealed class MonitoringOperationRepository : IRepository<MonitoringOperation>
  {
    private readonly MonitoringOperation[] _registeredOperations;

    public MonitoringOperationRepository(MonitoringOperation[] registeredOperations)
    {
      _registeredOperations = registeredOperations;
    }

    public int? Create(MonitoringOperation item)
    {
      throw new NotSupportedException();
    }

    public MonitoringOperation FindById(string id)
    {
      return _registeredOperations.Single(o => o.Name == id);
    }

    public IEnumerable<MonitoringOperation> Get()
    {
      return _registeredOperations.ToList();
    }

    public IEnumerable<MonitoringOperation> Get(Func<MonitoringOperation, bool> predicate)
    {
      return _registeredOperations.Where(predicate);
    }

    public void Remove(MonitoringOperation item)
    {
      throw new NotSupportedException();
    }

    public void Update(MonitoringOperation item)
    {
      throw new NotSupportedException();
    }
  }
}
