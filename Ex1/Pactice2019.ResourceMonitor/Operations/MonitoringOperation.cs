﻿using System.IO;

namespace Pactice2019.ResourceMonitor.Operations
{
  /// <summary>
  /// Represents a scanner callback operation.
  /// </summary>
  public abstract class MonitoringOperation
  {
    /// <summary>
    /// Initializes a new instance of scanner operation object.
    /// </summary>
    /// <param name="name">Name of operation.</param>
    protected MonitoringOperation(string name)
    {
      Name = name;
    }

    /// <summary>
    /// Name of operation.
    /// </summary>
    public string Name { get; }

    /// <summary>
    /// Performs operation on a scanner input.
    /// </summary>
    /// <param name="input">Input stream.</param>
    public abstract string Perform(Stream input);

    /// <summary>
    /// Indicates that operation can be performed on given stream.
    /// </summary>
    /// <param name="resource">Resource (file path or URI).</param>
    /// <returns>true - operation can be performed; otherwise - false.</returns>
    public abstract bool CanPerform(string resource);

    /// <summary>
    /// Target path or URI.
    /// </summary>
    public string TargetPath { get; set; }

    /// <summary>
    /// Indicates that operation is default.
    /// </summary>
    public bool IsDefault { get; protected set; }
  }
}