﻿using System;

namespace Pactice2019.ResourceMonitor.Services
{
  public sealed class ConsoleLogService : ILogService
  {
    public void Info(string message)
    {
      Console.WriteLine($"[{DateTime.Now}] {message}");
    }

    public void Debug(string message)
    {
      throw new NotImplementedException();
    }

    public void Error(string message)
    { 
      Console.WriteLine($"[{DateTime.Now}] {message}");
    }

    public void Error(Exception innerException)
    {
      Console.WriteLine(innerException);
    }

    public void Error(string message, Exception innerException)
    {
      Console.WriteLine(message);
      Console.WriteLine(innerException?.Message);
    }
  }
}