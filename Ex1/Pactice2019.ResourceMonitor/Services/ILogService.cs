﻿using System;

namespace Pactice2019.ResourceMonitor.Services
{
  public interface ILogService
  {
    void Info(string message);
    void Debug(string message);
    void Error(string message);
    void Error(Exception innerException);
    void Error(string message, Exception innerException);
  }
}