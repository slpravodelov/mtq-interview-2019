﻿using System.IO;
using Pactice2019.ResourceMonitor.Operations;

namespace Pactice2019.ResourceMonitor.Services
{
  public interface IReportingService
  {
    void Write(MonitoringOperation operation, string outputPath, string resourceName, string result);
    Stream GetStream(string reportUri);
    void CleanUp(string reportUri);
    void Configure(string reportUri);
  }
}
