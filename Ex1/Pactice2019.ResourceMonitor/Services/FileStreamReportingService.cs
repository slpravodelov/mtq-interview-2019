﻿using System.IO;
using Pactice2019.ResourceMonitor.Operations;

namespace Pactice2019.ResourceMonitor.Services
{
  public sealed class FileStreamReportingService : IReportingService
  {
    private static readonly object SSync = new object();

    public void Write(MonitoringOperation operation, string reportUri, string resourceName, string result)
    {
      using (var fs = new FileStream(reportUri, FileMode.Create, FileAccess.Write, FileShare.Write))
      {
        using (var sw = new StreamWriter(fs) {AutoFlush = true})
        {
          sw.WriteLine( /*$"[{DateTime.Now}]*/$"{operation.Name}-{Path.GetFileName(resourceName)}-{result}");
        }
      }
    }

    public Stream GetStream(string reportUri)
    {
      if (!File.Exists(reportUri))
      {
        throw new FileNotFoundException("Report file is not found.", reportUri);
      }
      return new FileStream(reportUri, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
    }

    public void CleanUp(string reportUri)
    {
      if (!File.Exists(reportUri)) return;
      File.Delete(reportUri);
    }

    public void Configure(string reportUri)
    {
      // TODO: Configuration logic...
    }
  }
}