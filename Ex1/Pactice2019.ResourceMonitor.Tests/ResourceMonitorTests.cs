﻿using NUnit.Framework;

using Pactice2019.ResourceMonitor.CLI.Windsor;
using Pactice2019.ResourceMonitor.Engines;
using Pactice2019.ResourceMonitor.Tests.Framework;

namespace Pactice2019.ResourceMonitor.Tests
{
  [TestFixture]
  public class ResourceMonitorTests : TestBase
  {
    [Test]
    public void CanInitializeFileStreamEngine()
    {
      ServiceContainer.Install(new ResourceMonitorInstaller());
      using (var engine = ServiceContainer.Resolve<IMonitoringEngine>())
      {
        Assert.IsInstanceOf<FileStreamMonitor>(engine);
      }
    }
  }
}
