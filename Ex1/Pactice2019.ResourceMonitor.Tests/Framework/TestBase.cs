﻿using System.IO;
using Castle.Windsor;
using NUnit.Framework;

namespace Pactice2019.ResourceMonitor.Tests.Framework
{
  /// <summary>
  /// Represents test base for Stream Scanner.
  /// </summary>
  public class TestBase
  {
    private const string DataSubDir = @"Framework\Data";
    private const string WatchDirName = @"Files";
    private const string SourceFileSubDir = @"Sources";
    private const string OutputFileName = @"Report.txt";

    public TestBase()
    {
      ServiceContainer = new WindsorContainer();

      var outputDir = Path.Combine(TestContext.CurrentContext.TestDirectory, DataSubDir);
      if (!Directory.Exists(outputDir))
      {
        Directory.CreateDirectory(outputDir);
      }

      OutputFile = Path.Combine(outputDir, OutputFileName);
      if (File.Exists(OutputFile))
      {
        File.Delete(OutputFile);
      }

      WatchDir = Path.Combine(TestContext.CurrentContext.TestDirectory, DataSubDir, WatchDirName);
      if (Directory.Exists(WatchDir))
      {
        Directory.Delete(WatchDir, true);
      }
      Directory.CreateDirectory(WatchDir);

      SourceFileDir = Path.Combine(TestContext.CurrentContext.TestDirectory, DataSubDir, SourceFileSubDir);
    }

    protected void CleanUpBase()
    {
      if (File.Exists(OutputFile))
      {
        File.Delete(OutputFileName);
      }

      if (Directory.Exists(WatchDir))
      {
        Directory.Delete(WatchDir, true);
      }

      Directory.CreateDirectory(WatchDir);
    }

    /// <summary>
    /// 
    /// </summary>
    protected IWindsorContainer ServiceContainer { get; }

    /// <summary>
    /// Output file (reporting).
    /// </summary>
    protected string OutputFile { get; }

    /// <summary>
    /// Directory to watch.
    /// </summary>
    protected string WatchDir { get; }

    /// <summary>
    /// Source test files.
    /// </summary>
    protected string SourceFileDir { get; }
  }
}
