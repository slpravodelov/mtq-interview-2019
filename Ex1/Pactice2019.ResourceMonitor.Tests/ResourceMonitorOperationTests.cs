﻿using System.IO;
using System.Linq;
using System.Threading.Tasks;

using NUnit.Framework;

using Pactice2019.ResourceMonitor.CLI.Windsor;
using Pactice2019.ResourceMonitor.Engines;
using Pactice2019.ResourceMonitor.Operations;
using Pactice2019.ResourceMonitor.Tests.Framework;

namespace Pactice2019.ResourceMonitor.Tests
{
  [TestFixture]
  public class ResourceMonitorOperationTests : TestBase
  {
    private IMonitoringEngine _engine;
    private const string ReportFormat = "{0}-{1}-{2}";

    private const string HtmlFileName = "HTMLFile.html";
    private const string ValidCssFileName = "ValidCssFile.css";
    private const string InvalidCssFileName = "InValidCssFile.css";
    private const string CustomFileName = "CustomFile.custom";

    [OneTimeSetUp]
    public void Init()
    {
      ServiceContainer.Install(new ResourceMonitorInstaller());

      _engine = ServiceContainer.Resolve<IMonitoringEngine>();
      _engine.Attach(WatchDir, OutputFile, false, true);
    }

    [OneTimeTearDown]
    public void Cleanup()
    {
      _engine.Dispose();
    }

    [SetUp]
    public void PerTestSetUp()
    {
      _engine.ClearReport();
    }

    [Test]
    public void CanResolveOperations()
    {
      var repository = ServiceContainer.Resolve<IRepository<MonitoringOperation>>();

      Assert.IsInstanceOf<MonitoringOperationRepository>(repository);

      var htmlFileOperation = repository.Get(o => o.CanPerform("somefile.html")).ToList();
      var cssFileOperation = repository.Get(o => o.CanPerform("somefile.css")).ToList();
      var customFileOperation = repository.Get(o => o.CanPerform("somefile.custom")).ToList();

      Assert.IsTrue(htmlFileOperation.Any());
      Assert.IsTrue(cssFileOperation.Any());
      Assert.IsTrue(customFileOperation.Any());
    }

    [Test]
    public void CanHandleHtmlFile()
    {
      File.Copy(Path.Combine(SourceFileDir, HtmlFileName), Path.Combine(WatchDir, HtmlFileName));
      Task.Delay(1000).Wait();

      var expexctedResultLine = string.Format(ReportFormat, typeof(CountHtmlDivOperation).Name, HtmlFileName, "7");
      var actualResultLine = "";

      using (var sr = new StreamReader(_engine.GetReportStream()))
      {
        actualResultLine = sr.ReadLine();
      }

      Assert.AreEqual(expexctedResultLine, actualResultLine);
    }

    [Test]
    public void CanHandleValidCssFile()
    {
      File.Copy(Path.Combine(SourceFileDir, ValidCssFileName), Path.Combine(WatchDir, ValidCssFileName));
      Task.Delay(1000).Wait();

      var expexctedResultLine = string.Format(ReportFormat, typeof(ValidateCssOperation).Name, ValidCssFileName, ValidateCssOperation.ValidCssMessage);
      var actualResultLine = "";

      using (var sr = new StreamReader(_engine.GetReportStream()))
      {
        actualResultLine = sr.ReadLine();
      }

      Assert.AreEqual(expexctedResultLine, actualResultLine);
    }

    [Test]
    public void CanHandleInValidCssFile()
    {
      File.Copy(Path.Combine(SourceFileDir, InvalidCssFileName), Path.Combine(WatchDir, InvalidCssFileName));
      Task.Delay(1000).Wait();

      var expexctedResultLine = string.Format(ReportFormat, typeof(ValidateCssOperation).Name, InvalidCssFileName, ValidateCssOperation.NotValidCssMessage);
      var actualResultLine = "";

      using (var sr = new StreamReader(_engine.GetReportStream()))
      {
        actualResultLine = sr.ReadLine();
      }

      Assert.AreEqual(expexctedResultLine, actualResultLine);
    }

    [Test]
    public void CanHandleCustomFile()
    {
      File.Copy(Path.Combine(SourceFileDir, CustomFileName), Path.Combine(WatchDir, CustomFileName));
      Task.Delay(1000).Wait();

      var expexctedResultLine = string.Format(ReportFormat, typeof(ReadPunctuationOperation).Name, CustomFileName, "6");
      var actualResultLine = "";

      using (var sr = new StreamReader(_engine.GetReportStream()))
      {
        actualResultLine = sr.ReadLine();
      }

      Assert.AreEqual(expexctedResultLine, actualResultLine);
    }
  }
}