SELECT 
	[ClientName], 
	MONTH([Date])	AS [Month],
	SUM([Amount])	AS [SumAmount]
FROM [ClientSupply]
WHERE [Date] BETWEEN '2017-01-01' AND '2017-12-31'
GROUP BY [ClientName], 	MONTH([Date])
ORDER BY [ClientName] DESC, [SumAmount], MONTH([Date])