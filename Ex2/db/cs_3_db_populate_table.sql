SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

INSERT INTO dbo.ClientSupply ( ClientName, SupplyNumber, Date, Amount ) VALUES ('ООО Ромашка',111,'2017-01-01',100.0000);
INSERT INTO dbo.ClientSupply ( ClientName, SupplyNumber, Date, Amount ) VALUES ('ООО Ромашка',222,'2017-01-05',150.0000);
INSERT INTO dbo.ClientSupply ( ClientName, SupplyNumber, Date, Amount ) VALUES ('ООО Ромашка',333,'2017-02-07',200.0000);
INSERT INTO dbo.ClientSupply ( ClientName, SupplyNumber, Date, Amount ) VALUES ('ИП Лютик',444,'2017-01-02',110.0000);
INSERT INTO dbo.ClientSupply ( ClientName, SupplyNumber, Date, Amount ) VALUES ('ИП Лютик',555,'2017-04-05',120.0000);
INSERT INTO dbo.ClientSupply ( ClientName, SupplyNumber, Date, Amount ) VALUES ('ООО Ромашка',777,'2018-03-10',100.0000);
INSERT INTO dbo.ClientSupply ( ClientName, SupplyNumber, Date, Amount ) VALUES ('ИП Лютик',888,'2018-04-12',210.0000);
GO
